# Einführung ITplusX


## Aufgabe

- Gehe zu einem Mitarbeiter und lass dir von ihm ein bisschen etwas über ITplusX erzählen - vergiss dabei nicht, dir ein
  paar Notizen für deinen Praktikumsbericht zu machen.

---

[< Zum vorherigen Thema](./1-markdown.md) | [Zum nächsten Thema >](./3-basics-network-engineering-and-administration.md)

[<<< Zurück zur Übersicht](../README.md)
