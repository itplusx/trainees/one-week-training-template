# Hands on Hardware

Nachdem du dir nun die Grundlagen erarbeitet hast, wird es nun etwas praktischer und du darfst ein wenig an einem PC herum
schrauben. Gehe dazu zu Stefan und lasse dich von ihm einweisen.


## Aufgaben

- Schraube unter der Anweisung von Stefan einen PC auseinander.
- Versuche, die einzelnen Komponenten zu identifizieren.
- Fotografiere die wichtigsten Komponenten.
- Lege im Ordner `docs` einen neuen Ordner `images` an und lege darin die Fotos ab.

---

[< Zum vorherigen Thema](./3-basics-network-engineering-and-administration.md) | [Zum nächsten Thema >](./5-setup-router.md)

[<<< Zurück zur Übersicht](../README.md)
