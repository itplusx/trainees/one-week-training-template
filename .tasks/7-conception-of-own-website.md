# Konzeption einer eigenen Website

Nach der kleinen Einführung in HTML & CSS darfst du nun ab heute bis zum Ende deines Praktikums Stück für
Stück deine eigene kleine Website basteln. 

Die Themenwahl ist ganz dir selbst überlassen. Wähle allerdings am besten etwas, für das du bereits ein paar Inhalte hast
(zum Beispiel deinen Praktikumsbericht) oder ein Thema, bei dem du weißt, wie du schnell an Inhalte kommst. 


## Grobkonzept

Bevor es aber an die Umsetzung geht, gilt es vorab noch ein paar Überlegungen anzustellen. Mit der Konzeption im Vorfeld
steht und fällt jedes Projekt. Je ausgearbeiteter das Konzept, umso besser und schneller die Umsetzung - so auch bei uns
in der Webentwicklung.


### Aufgaben

- Erstelle einen Ordner `concept`.
- Erstelle im Ordner `concept` eine Datei `objectives.md`.
- Beantworte in dieser Datei zunächst folgende Fragen:
  - Was sind die Ziele der Website?
  - Wer ist die Zielgruppe der Website?
- Erstelle im Ordner `concept` eine Datei `content.md`.
- Bereite grob die Text-Inhalte der Website in der Seite `content.md` vor und versuche diese bestenfalls gleich ein wenig
  zu strukturieren und hierarchisch zu Gliedern (_eine(!) Gesamtüberschrift/ein Gesamtthema, mehrere Themen(-überschriften),
  mehrere Unterthemen(-überschriften),..._).
- Suche ein paar zu den Texten passende Bilder und lege diese in einem neuen Ordner `concept/images` ab.
- Platziere die Bilder an den entsprechenden Stellen in der Datei `content.md`.

---


## Feinkonzept

Im Anschluss geht es nun an ein etwas konkreteres, visuelles Konzept, das du erstmal auf Papier zeichnen sollst. Damit du
ein paar Anhaltspunkte hast vorab ein paar Anforderungen, die deine Website später erfüllen soll.


### Anforderungen

- Layout:
  - Headerleiste
  - Header
  - Hauptbereich
  - Footer
- Komponenten:
  - Navigation in Headerleiste (Verlinkungen zu einzelnen Seiten der Website, einzelne Links nebeneinander)
  - Teaser in Header bestehend aus
    - Seitentitel
    - Hintergrundbild
  - Einzelne Sektionen pro Unterthema im Hauptbereich (optisch mindestens durch größere Abstände voneinander getrennt)
  - Footer bestehend aus 
    - Einem kurzen Copyright-Text 
    - Einem Link zu einer Impressumsseite


### Aufgaben

- Zeichne mit Stift & Papier ein grobes Layout der Website, bei dem mindestens folgendes grob(!) ersichtlich ist:
  - Wie wird die Headerleiste dargestellt?
    - Volle Seitenbreite (auch bei großen Displays) oder gibt es eine Maximalbreite? 
    - Wie wird die Hauptnavigation im Header dargestellt?
      - Volle Headerbreite oder (abweichende) Maximalbreite?
      - Wie sind die einzelnen Navigationspunkte angeordnet? 
  - Wie wird der Teaser im Header dargestellt?
    - Volle Seitenbreite oder Maximalbreite?
    - Wie hebt sich der Seitentitel vom Teaserbild ab?
    - Wie wird der Seitentitel dargestellt?
      - Links, rechts oder mittig angeordnet?
      - Größe?
      - Schriftauszeichnung (fett, kursiv, unterstrichen, Großbuchstaben,
        Kapitälchen,...)?
  - Wie wird der Hauptbereich dargestellt?
    - Volle Seitenbreite oder Maximalbreite?
  - Wie groß sind etwa die Abstände zwischen den einzelnen Sektionen im
    Hauptbereich
  - Wie wird der Footer ansich dargestellt?
    - Volle Seitenbreite oder Maximalbreite?
    - Wie sind die Footerinhalte angeordnet?
- Fotografiere oder scanne deine Zeichnung(en) ein und lade sie in den Ordner `concept/images` hoch.
- Lass dir von Vaci beide Konzepte abnehmen.

---

[< Zum vorherigen Thema](./6-training-of-html-and-css.md) | [Zum nächsten Thema >](./8-create-own-website.md)

[<<< Zurück zur Übersicht](../README.md)
