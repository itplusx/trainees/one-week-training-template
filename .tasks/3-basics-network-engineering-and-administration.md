# Grundlagen Hardware und Netzwerktechnik

Wie du hoffentlich im Zuge der 
[Einführung bei ITplusX](2-introduction-itplusx.md) erfahren hast, besteht unser
Tagesgeschäft aus den zwei Bereichen _Internetagentur_ und _Systemhaus_.

Der Bereich, den du als erstes ein wenig näher kennenlernen wirst, ist das 
_Systemhaus_. Bevor es hier jedoch ein wenig praktischer wird, solltest
du dir vorweg ein paar Grundlagen erarbeiten.


## Aufgaben

- Lese dir den Artikel [_Wie funktioniert ein Computer?_](https://www.giga.de/downloads/windows-10/specials/wie-funktioniert-ein-computer-erklaerung-fuer-laien-profis/)
  durch und beantworte (knapp) in `docs/notes.md` die folgenden Fragen:
  - Was sind die wichtigsten Komponenten eines Computers?
  - Was ist deren jeweilige Funktion?
- Lese dir den Artikel [_Der ultimative Netzwerkguide_](http://www.olivergast.de/blog/2011/04/16/netzwerkguide_1/) durch
  und beantworte (max. 2 - 3 Sätze) in `docs/notes.md` die folgenden Fragen:
  - Wie heißen die beiden grundlegenden Netzwerktypen und wie unterscheiden sie sich?
  - Wie heißt die Adresse eines Gerätes in einem Netzwerk und wie sieht diese aus?
  - Was ist neben dieser Adresse und einem bestehenden (physischen) Netzwerk außerdem noch die Voraussetzung dafür, dass
    sich zwei Geräte erreichen können?
  - Wie nennt man das Bindeglied zwischen einem lokalen Netzwerk und dem World Wide Web?
  - Wofür steht _DNS_ und was sind die Aufgaben eines DNS-Servers?
  - Wofür steht _DHCP_ und was macht es?
  - Welche Hardware-Komponenten sind für den Betrieb eines lokalen Netzwerkes notwendig?


---

[< Zum vorherigen Thema](./2-introduction-itplusx.md) | [Zum nächsten Thema >](./4-hands-on-hardware.md)

[<<< Zurück zur Übersicht](../README.md)
