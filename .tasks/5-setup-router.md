# Einrichten eines Routers

Weiter geht's nun mit der Konfiguration eines Routers. Auch hierfür brauchst du wieder eine kleine Unterweisung von Stefan.


## Aufgaben

- Melde dich auf der Weboberfläche des Routers an.
- Ändere das Passwort des Routers.
- Ändere die WLAN SSID das WLAN Passwort.
- Melde dich mit einem Endgerät (Laptop, Tablet, Smartphone) im WLAN an.
- Ändere die DHCP-Range von `192.168.1.0/24` auf `10.161.50.0/24`.
- Stelle den WAN-Port auf DHCP um und verbinde dich per LAN mit unserem internen Netzwerk.
- Weise mittels MAC Bind einem Client eine fixe interne IP zu.

---

[< Zum vorherigen Thema](./4-hands-on-hardware.md) | [Zum nächsten Thema >](./6-training-of-html-and-css.md)

[<<< Zurück zur Übersicht](../README.md)
