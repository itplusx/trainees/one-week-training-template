# Erstellen einer eigenen Website

Nachdem du nun ein Konzept erarbeitet hast, geht es nun an die Umsetzung.

Damit du möglichst viele verschiedene Themen abdecken kannst, haben wir ein paar Anforderungen an deine Website erstellt,
die deine Seite erfüllen _kann_. Diese sind nur als Leitfaden gedacht und nicht alle davon _müssen_ umgesetzt werden.
Schaue einfach, wie weit du kommst und überlege selbst, was du davon umsetzen möchtest.

Die Anforderungen, die mit _**BONUS:**_ gekennzeichnet sind, sind meistens etwas knackiger. Diese kannst du dann angehen,
wenn du alle anderen weitestgehend umgesetzt hast.


## Anforderungen

- Seiten:
  - Eine Start-/Willkommensseite
  - Mehrere Unterseiten
  - Eine Impressumsseite
- Layout:
  - Das Layout soll folgende Bereiche enthalten:
    - Headerleiste
    - Header
    - Hauptbereich
    - Footer
  - Korrekte Verwendung der Tags `<header>`, `<main>` und `<footer>`
- Komponenten:
  - `Navigation`:
    - Platziert in Headerleiste
    - Navigation zu einzelnen Seiten
    - Links nebeneinander
    - Immer am oberen Bildschirmrand fixiert (auch sichtbar, wenn nach unten gescrollt wurde)
    - Der Link, über dem man den Cursor bewegt, soll sich optisch von den anderen abheben
      - _**BONUS:** Anpassungen der (Hintergrund-)Farbe mit einem kurzen zeitlichen Übergang_
    - Der Link zur aktuellen Seite soll sich optisch von den anderen abheben
    - Verwendung des Tags `<nav>`
    - _**BONUS:** Verwendung der Tags `<ul>` und `<li>`_
  - `Hero` (Seitenteaserbild und darüberliegender Text):
    - Platziert im Seitenheader
    - Bestehend aus
      - Dem jeweiligen Seitentitel
        - _**BONUS:** Gibt durch Abstände die Größe des Heros vor_
      - Einem Teaserbild im Hintergrund
        - _**BONUS:** Bild füllt immer die gesamte Höhe und Breite des Heros aus_
  - `Footer`:
    - Platziere einen kurzen Copyright-Text mit der aktuellen Jahreszahl im Footer.
    - Platziere einen Link zur Impressumsseite im Footer.
  - `Section` (separate Abschnitte für inhaltlich getrennte Themen):
    - Verwende pro Unterthema eine `Section`-Komponenten im Seitenhauptbereich.
    - Trenne `Section`-Komponenten optisch voneinander (z.B. durch größere Abstände, Trennlinien,...).
    - _**BONUS:** Gebe hintereinanderfolgende `Section`-Komponenten mit abwechselnden Hintergrundfarben (z.B.: weiß, hellgrau)
      aus._
  - `TextWithMedia` (Text und Bild):
    - Verwende eine `TextWithMedia`-Komponente mindestens einmal irgendwo innerhalb einer `Section` (eines Unterthemas).
    - _**BONUS:** Ergänze das Bild um eine Bildunterschrift (Stichwort: "figure" und "figcaption")_
- Markup:
  - Verwende korrekte HTML-Elemente für Paragraphen und Überschriften bei Textinhalten.
  - Verwende `<br>`-Tags ausschließlich für Umbrüche in Fließtexten (nicht für Abstände).
- Globaler Style:
  - Passe die Schriftart gesamten Seite an.
- Verwendung von Klassen:
  - Verwende keine Klassen unterschiedlicher Komponenten auf dem selben html-Element.
- Struktur:
  - Verwende einzelne CSS-Dateien für voneinander unabhängige Komponenten (z.B. `Header`, `Teaser`, `Section`, `Footer`),
    sowie für `Base`-Styling und führe diese in einer Datei zusammen.
  - Style, der auf globale Elemente wie `:root`, `<html>`, `<body>`, `<p>`, `<a>`, `<h1>`, `<h2>`,... angewandt werden soll,
    soll in der Datei `styles/Base.css` definiert werden.
- Inhalte:
  - Generiere ein Impressum mit Hilfe des [Impressum-Generators](https://www.impressum-generator.de/) und binde das Ergebnis
    auf der Seite `pages/imprint.html` ein.
- Sonstiges:
  - Nutze [relative Pfade](https://www.w3schools.com/Html/html_filepaths.asp) für sämtliche Links, die auf andere Seiten
    deiner Websites verlinken.
  - Öffne Links zu externen Seiten in einem neuen Fenster/Tab.

---


## Aufgabe

- Lege in deinem Projekt auf oberster Ebene eine Datei `index.html` 
  (Start-/Willkommensseite) an
- Lege in deinem Projekt einen Ordner `pages` an, in dem alle anderen 
  HTML-Dateien (Unterseiten) abgelegt werden
- Lege im Ordner `pages` eine Datei `imprint.html` an
- Lege in deinem Projekt einen Ordner `styles` an, in dem sämtliche CSS-Dateien
  abgelegt werden
- Lege im Ordner `styles` eine Datei `index.css` an, welche ausschließlich 
  alle anderen CSS-Dateien "vereint" und als einzige in den HTML-Dateien
  importiert wird (siehe Abschnitt "External References" auf 
  [dieser Seite](https://www.w3schools.com/html/html_css.asp))
- Erstelle auf dieser Basis und mit den im 
  [Konzept](./6-conception-of-own-website.md) definierten Inhalten eine 
  Website, die mehrere der oben genannten **Anforderungen** erfüllt

---

[< Zum vorherigen Thema](./7-conception-of-own-website.md) |

[<<< Zurück zur Übersicht](../README.md)
