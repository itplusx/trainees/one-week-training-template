# Markdown

Bevor es du mit den eigentlichen Aufgaben anfängst, ist es sinnvoll dich 
zunächst mit [Markdown](https://de.wikipedia.org/wiki/Markdown) auseinander zu
setzen. 

Markdown ist eine einfache, weit verbreitete Auszeichnungssprache, über die du 
vielleicht auch schon mal gestolpert bist, die häufig zu Dokumentationszwecken 
verwendet wird. Mit einer relativ zugänglichen Schreibweise kann mit Hilfe von 
Markdown schnell ein ausreichend formatiertes Dokument angelegt werden.


## Aufgaben

- Lege in deinem Projekt einen Ordner `docs` an.
- Lege in dem Ordner `docs` eine Datei `notes.md` an.
- Mach dich ein wenig mit der [Markdown-Syntax](https://github.com/adam-p/markdown-here/wiki/Markdown-Here-Cheatsheet) vertraut
  und spiele in `notes.md` ein wenig damit herum.
- **ONGOING:** Streiche Aufgaben die du erledigt hast in der jeweiligen Markdown-Datei durch (die Dateien liegen im Ordner
  `.tasks` ab).
- **ONGOING:** Nutze die Datei `notes.md` während der Woche um dir Notizen zu den Dingen, die du lernst zu machen und Links
  festzuhalten.
- Lege in dem Ordner `docs` eine Datei `internship-report.md` an.
- **ONGOING:** Nutze jeden Tag die letzte halbe Stunde vor Feierabend dafür, deine Notizen aus `notes.md` in der Datei
  `internship-report.md` ein wenig auszuformulieren - solltest du tagsüber mal etwas Luft haben, kannst du das natürlich
  auch gerne vorziehen. 

---


## Hinweise

Solange du hier bei uns in GitLab arbeitest, wirst du Markdown-Dateien ohne weiteres richtig formatiert darstellen können.
Außerhalb von GitLab benötigst du jedoch andere Tools, die deine Datei richtig interpretieren.

Hier ein paar Möglichkeiten:

- [Visual Studio Code](https://code.visualstudio.com/)
- [Online Tool: Dillinger](https://dillinger.io/)
- [Chrome Extension: Markdown Viewer](https://chrome.google.com/webstore/detail/markdown-viewer/ckkdlimhmcjmikdlpkmbgfkaikojcbjk)

---

[Zum nächsten Thema >](./2-introduction-itplusx.md)

[<<< Zurück zur Übersicht](../README.md)
