# Einarbeitung in HTML & CSS

Nachdem du nun einen kleinen Einblick in den Bereich Systemhaus bekommen hast, geht es nun weiter mit unserem zweitem Geschäftsfeld,
der Internetagentur.

Eine wichtige Grundlage für diesen Bereich sind ein paar Grundkenntnisse in diversen (Frontend-)Sprachen. Die wichtigsten
davon sind **HTML**, **CSS** und **JavaScript**.
 
**HTML** ist zunächst einmal für das Grundgerüst/"Skelett" einer Website verantwortlich. Das Aussehen (und einfache Animationen)
werden mit **CSS** definiert. Funktionen und Berechnungen erfolgen dann letztendlich mit Hilfe von **JavaScript**.

Für dich relevant sind zunächst einmal nur **HTML** & **CSS**. Diese Sprachen sind grundsätzlich auch gar nicht so schwer
zu lernen, und die Grundlagen kannst du dir schon bis heute Abend erarbeiten.

Aufgaben, die mit _**BONUS:**_ gekennzeichnet sind, sind optional. Diese kannst du dann angehen, wenn du alle anderen soweit
abgeschlossen hast.


## Aufgaben

- Schließe alle Übungen des Tutorials im Bereich [Basic HTML and HTML5](https://learn.freecodecamp.org/responsive-web-design/basic-html-and-html5/) 
  der [Lernplattform freeCodeCamp](https://learn.freecodecamp.org/) ab.
- Schließe alle Übungen des Tutorials im Bereich [Basic CSS](https://learn.freecodecamp.org/responsive-web-design/basic-css/) 
  der [Lernplattform freeCodeCamp](https://learn.freecodecamp.org/) ab.


---


## Hinweise

Neben [freeCodeCamp](https://www.freecodecamp.org/) findet man online noch jede Menge weiterer Tutorials. Hier noch ein
paar weitere Möglichkeiten:
- [w3schools](https://www.w3schools.com)
  - [HTML Tutorial](https://www.w3schools.com/html/default.asp)
  - [HTML Excercises](https://www.w3schools.com/html/exercise.asp)
  - [CSS Tutorial](https://www.w3schools.com/css/default.asp)
  - [CSS Excercises](https://www.w3schools.com/css/exercise.asp)
  - [JS Tutorial](https://www.w3schools.com/js/default.asp)
  - [JS Excercises](https://www.w3schools.com/js/exercise_js.asp)
- [Codecademy](https://www.codecademy.com/) (Account benötigt)
  - [Introduction to HTML](https://www.codecademy.com/learn/learn-html) (free)
  - [Learn CSS](https://www.codecademy.com/learn/learn-css) (free)
  - [Make a Website](https://www.codecademy.com/learn/make-a-website) (free)

Außerdem findet man auf der Seite [hackr.io](https://hackr.io/) noch jede Menge weitere Alternativen und Tutorials für andere
Sprachen.

---

[< Zum vorherigen Thema](./5-setup-router.md) | [Zum nächsten Thema >](./7-conception-of-own-website.md)

[<<< Zurück zur Übersicht](../README.md)
