# Wochenpraktikum


## Hi!

Willkommen zu deinem Praktikum bei ITplusX! Schön, dass du da bist! 

Um dir einen guten Einblick in unser Tagesgeschäft zu geben, haben wir dieses Projekt hier angelegt. Es soll dich durch deine
Praktikumszeit bei uns führen, dir unser Tagesgeschäft ein wenig näher bringen und dir die Möglichkeit geben ein paar Grundlagen
aus den Bereichen Systemadministration und Webentwicklung zu lernen.

Im nächsten Abschnitt findest du einen groben Zeitplan mit einzelnen Themen. Für einen Teil der Themen muss dir jemand von
uns zur Seite stehen, einige kannst du allerdings auch komplett selbstständig angehen.

Das ganze Projekt kannst du am Ende der Woche mit nach Hause nehmen. Wenn alles gut läuft hast du damit auch deinen Praktikumsbericht
bereits zum Teil abgehakt.

Falls du irgendwelche Fragen hast, kannst du dich natürlich gerne jederzeit an einen von uns wenden. 

Viel Spaß und gutes Gelingen!

---


## Tag 1

- [Markdown](.tasks/1-markdown.md)
- [Einführung ITplusX](.tasks/2-introduction-itplusx.md)
- [Grundlagen Netzwerktechnik und -administration](.tasks/3-basics-network-engineering-and-administration.md)

---


## Tag 2

- [Hands on Hardware](.tasks/4-hands-on-hardware.md)
- [Einrichten eines Routers](.tasks/5-setup-router.md)

---


## Tag 3

- [Einarbeitung in HTML & CSS](.tasks/6-training-of-html-and-css.md)
- [Konzeption einer eigenen Website](.tasks/7-conception-of-own-website.md)

---


## Tag 4 & 5

- [Erstellen einer eigenen Website](.tasks/8-create-own-website.md)
